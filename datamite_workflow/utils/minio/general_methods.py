"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import pandas as pd
from visions.functional import infer_type
from typing import List, Dict, Union

from datamite_workflow.utils.minio.custom_set import CustomSet


class GeneralMethods:
    MAX_SAMPLE_SIZE = 20
    typeset = CustomSet()

    @staticmethod
    def data_types(dataset: Union[pd.Series, pd.DataFrame]) -> List[Dict[str, str]]:
        """
            Obtains the data types of each column in df.

            Return
            ------
            _: List[Dict[str, str]]
                List of dictionaries containing the column name (`column_name`), and its type (`type`).
            """
        # get model to infer the data types of each column
        # infer the data type
        if isinstance(dataset, pd.Series):
            return [GeneralMethods.__infer_data_types(srs=dataset, typeset=GeneralMethods.typeset,
                                                      column_name='' if dataset.name is None else dataset.name)]

        return [GeneralMethods.__infer_data_types(
            dataset[column_name], typeset=GeneralMethods.typeset, column_name=column_name) for column_name in dataset]

    @staticmethod
    def __infer_data_types(
            srs: pd.Series, typeset: "CustomSet", column_name: str) -> Dict[str, str]:
        """
        Infer the data type of the data in a column.

        Parameters
        ----------
        srs: :obj:`pandas.Series`
            Data in which the computations are done
        column_name: str
            Name of the data that is going to be processed.
        typeset: :obj:`CustomSet`
            Object of the library `visions` which helps to infer the data types of each column.

        Return
        ------
        _: dict
            Dictionary containing the name of the column ('column_name'), its type ('type') and its position
            ('position') in the dataset.
        """
        # drop nan values
        srs.dropna(inplace=True)
        # infer data type (get a sample for that)
        tt = str(
            infer_type(srs.iloc[:GeneralMethods.MAX_SAMPLE_SIZE] if srs.size > GeneralMethods.MAX_SAMPLE_SIZE else srs,
                       typeset))
        # check if the datatype could not be inferred
        if tt in ['General', 'Object'] and srs.shape[0] > GeneralMethods.MAX_SAMPLE_SIZE:
            for index in range(1, 3):
                tt = str(infer_type(
                    srs.iloc[index * GeneralMethods.MAX_SAMPLE_SIZE:(index + 1) * GeneralMethods.MAX_SAMPLE_SIZE],
                    typeset))
                if tt not in ['General', 'Object']:
                    break
        # return
        return {'column_name': column_name, 'method_name': 'data_types', 'data_type': 'String',
                'value': str(tt)}
