"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

from pydantic_settings import BaseSettings
from pydantic import Field, field_validator, ConfigDict

from typing import Optional
import logging

# Setup logger
logger = logging.getLogger(__name__)

class Config(BaseSettings):
    streaming_service: str = Field(default=None, alias="STREAMING_SERVICE")
    governance_service: str = Field(default=None, alias="GOVERNANCE_URL")
    quality_service: str = Field(default=None, alias="QUALITY_URL")
    sampling_limit: int = Field(default=10000, alias="SAMPLING_LIMIT")

    model_config = ConfigDict(
        populate_by_name=True,
    )

    @classmethod
    def _remove_trailing_slash(cls, v):
        if isinstance(v, str) and v.endswith('/'):
            return v[:-1]
        return v

    @field_validator('streaming_service', 'governance_service', 'quality_service',
                     mode='before')
    def check_urls(cls, v):
        return cls._remove_trailing_slash(v)

    def to_dict(self) -> dict:
        # Create a dictionary for logging the configuration details
        return {
            "streaming_service": self.streaming_service,
            "governance_service": self.governance_service,
            "quality_service": self.quality_service,
            "sampling_limit": self.sampling_limit,
        }

# Instantiate the configuration
config = Config()

# Log the configuration details (useful for debugging)
logger.info("Configuration loaded: %s", config.to_dict())
