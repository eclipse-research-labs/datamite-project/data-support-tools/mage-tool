"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
from typing import Optional
from uuid import UUID

import requests, logging
from pydantic import BaseModel
from datamite_workflow.utils.streaming import config


class StreamingError(BaseModel):
    status_message: Optional[str] = None


logger = logging.getLogger(__name__)


def notify_error(streaming_id: UUID, error_message: str):
    error_dto = StreamingError(status_message=error_message)
    response = requests.put(
        f"{config.streaming_service}/streamings/{streaming_id}/error",
        json=error_dto.dict(),
        timeout=20
    )
    if response.status_code != 200:
        logger.error(f"Failed to notify error: {response.status_code} - {response.text}")
    else:
        logger.info("Error notification sent successfully")


def notify_success(streaming_id: UUID):
    response = requests.put(
        f"{config.streaming_service}/streamings/{streaming_id}/finish",
        timeout=20
    )
    if response.status_code != 200:
        logger.error(f"Failed to notify success: {response.status_code} - {response.text}")
