"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
# artifact_management/services/artifact_service.py
import uuid
from datetime import datetime
from typing import List, Dict
from uuid import UUID
import pandas as pd

from ...artifact_management.clients import streaming_client
from ...artifact_management.clients.streaming_client import (ArtifactDto, ArtifactParametersDto, ColumnInfo, ArtifactColumns)
from ....minio.general_methods import GeneralMethods


def get_or_create_active_artifact(streaming_id: UUID, size: int) -> ArtifactDto:
    artifacts = streaming_client.get_active_artifacts(streaming_id)
    if artifacts:
        return artifacts[0]
    else:
        return streaming_client.create_artifact(streaming_id, ArtifactParametersDto(size=size))


def consolidate_artifact(streaming_id: UUID, artifact_id: UUID, path: str):
    df = pd.read_json(path, lines=True)
    column_data_types: List[Dict[str, str]] = GeneralMethods.data_types(df)
    num_rows = df.shape[0]
    columns_info = []
    for index, column_data_type in enumerate(column_data_types):
        column_name = column_data_type['column_name']
        column_info = ColumnInfo(
            name=column_name,
            description=None,
            summary=None,
            dataType=column_data_type['value'],
            position=index
        )
        columns_info.append(column_info)

    artifact_columns = ArtifactColumns(
        createdAt=int(datetime.now().timestamp()),
        numRecords=num_rows,
        columns_info=columns_info
    )
    return streaming_client.consolidate_artifact(streaming_id, artifact_id, artifact_columns)

