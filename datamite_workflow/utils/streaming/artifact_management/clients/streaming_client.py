"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import requests
from uuid import UUID
from typing import List, Optional
from pydantic import BaseModel
import enum

from ... import config


class ArtifactStatusEnum(str, enum.Enum):
    pending = "pending"
    to_consolidate = "to_consolidate"
    consolidated = "consolidated"


class LocationParameters(BaseModel):
    bucket_name: str
    #  key_name: Optional[str]
    key_path: str


class ArtifactInfoDto(BaseModel):
    artifact_id: UUID
    resource_name: str
    status: ArtifactStatusEnum


class ArtifactDto(BaseModel):
    info: ArtifactInfoDto
    location: LocationParameters


class ArtifactParametersDto(BaseModel):
    size: int


class ColumnInfo(BaseModel):
    name: str
    description: Optional[str] = None
    summary: Optional[str] = None
    dataType: str
    position: int


class ArtifactColumns(BaseModel):
    createdAt: int
    numRecords: int
    columns_info: List[ColumnInfo]


BASE_URL = config.streaming_service


def get_active_artifacts(streaming_id: UUID) -> List[ArtifactDto]:
    response = requests.get(f"{BASE_URL}/streamings/{streaming_id}/artifacts", params={"only_active": True})
    response.raise_for_status()
    return [ArtifactDto(**artifact) for artifact in response.json()]


def create_artifact(streaming_id: UUID, params: ArtifactParametersDto) -> ArtifactDto:
    response = requests.post(f"{BASE_URL}/streamings/{streaming_id}/artifacts",
                             json=params.dict())
    response.raise_for_status()
    return ArtifactDto(**response.json())


def consolidate_artifact(streaming_id: UUID, artifact_id: UUID, columns_info: ArtifactColumns) -> ArtifactDto:
    response = requests.put(f"{BASE_URL}/streamings/{streaming_id}/artifacts/{artifact_id}/consolidate",
                            json=columns_info.dict())
    response.raise_for_status()
    return ArtifactDto(**response.json())
