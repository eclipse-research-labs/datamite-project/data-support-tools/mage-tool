"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import json
from datetime import datetime
from typing import Any, List, Optional, Union

import random
import logging
import requests
from requests.exceptions import Timeout
import os
import math
from pydantic import BaseModel
from pydantic import Field

from datamite_workflow.utils.streaming import config

logger = logging.getLogger(__name__)


class EvaluateDataRequestDTO(BaseModel):
    dataset_id: str = Field(..., description="ID of the dataset")
    column_name: str = Field(..., description="Name of the column")
    data_type: str = Field(..., description="Type of the column")
    values: List[Optional[Union[str, int, float, bool]]] = Field(..., description="List of values which can be of "
                                                                                  "any type, including null")


class ProfilingItem(BaseModel):
    ddqv_has_parameters: List = Field(..., alias="ddqv_hasParameters", description="List of parameters for the DDQV.")
    dqv_computed_on: str = Field(..., alias="dqv_computedOn", description="The scope of the measurement (e.g., "
                                                                          "DATASET, column).")
    dqv_is_measurement_of: str = Field(..., alias="dqv_isMeasurementOf", description="What is being measured.")
    dqv_value: Optional[Union[str, int, float, bool, list]] = Field(..., alias="dqv_value",
                                                                    description="The value of the "
                                                                                "measurement.")
    rdf_datatype: Optional[str] = Field(..., alias="rdf_datatype", description="The datatype of the measurement.")


class EvaluateDataResponseDTO(BaseModel):
    dataset_id: str = Field(..., description="ID of the dataset.")
    column_name: str = Field(..., description="Name of the column.")
    profiling: List[ProfilingItem] = Field(..., description="List of profiling information.")
    timestamp: Optional[datetime] = Field(..., description="Timestamp of the profiling result.")


def metric_value(dqv_value, rdf_datatype):
    # Convertir el metric_value basado en rdf_datatype
    value = dqv_value
    try:
        if rdf_datatype == "Integer":
            value = int(dqv_value)
        elif rdf_datatype == "Float":
            value = float(dqv_value)
        elif rdf_datatype == "String":
            value = str(dqv_value)
        elif rdf_datatype == "Map<String,String>":
            value = json.loads(dqv_value)
        elif rdf_datatype == "List<Map<String,String>>":
            value = json.loads(dqv_value)
    except Exception as e:
        message = f"Failed to convert a value in evaluate_data: {rdf_datatype} {dqv_value} - {e}"
        logger.error(message)
    return value




def evaluate_data(data, dataset: str, resource_name: str, column_name: str, column_type: str):
    print("evaluate_data(data, resource_name: str, column_name: str, column_type: str):")
    print(f'data, {resource_name}, {column_name}, {column_type}')
    data_ready = list(map(lambda x: None if isinstance(x, float) and math.isnan(x) else x, data))
    if len(data_ready) > config.sampling_limit:
        data_ready = random.sample(list(data_ready), config.sampling_limit)
    payload = EvaluateDataRequestDTO(
        dataset_id=f'{dataset}_{column_name}',    # f'{filename_without_extension}{random.randint(1000, 9999)}_{column_name}',
        column_name=column_name,
        data_type=column_type,
        values=data_ready
    )
    print(f"Calling evaluate_data: {payload.model_dump_json()}")
    try:
        headers = {"content-type": "application/json"}
        response = requests.post(
            f"{config.quality_service}/evaluate-data",
            json=payload.model_dump(),
            headers=headers,
            timeout=20
        )
        print("Called evaluate_data")
    except Timeout:
        print("Timeout")
        return None
    except Exception as e:
        message = f"Failed to evaluate_data error: {e} "
        # logger.error(message)
        print(message)
        return None
    if response.status_code != 200:
        if response.status_code == 500:
            print(f"Error 500: {response.text}")
        else:
            message = f"Failed to evaluate_data error: {response.status_code} - {response.text}"
            # logger.error(message)
            print(message)
        return None
    else:
        logger.info("Evaluate_data sent successfully")

    response_data = response.json()
    evaluate_response: EvaluateDataResponseDTO = EvaluateDataResponseDTO(**response_data)
    result = {}
    for p in evaluate_response.profiling:
        result[p.dqv_is_measurement_of] = str(p.dqv_value)  # metric_value(p.dqv_value, p.rdf_datatype)
    return result
