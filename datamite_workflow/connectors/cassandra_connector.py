"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

from .base_connector import BaseConnector
from .connector_factory import ConnectorFactory
from .config.connector_configuration import ConnectorConfiguration
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
import logging
import random

logger = logging.getLogger(__name__)


@ConnectorFactory.register("cassandra")
class CassandraConnector(BaseConnector):

    def __init__(self, config: ConnectorConfiguration):
        self.keyspace = None
        self.password = None
        self.username = None
        self.port = None
        self.contact_points = None
        self.session = None
        self.cluster = None
        super().__init__(config)

    def load_config(self, config: ConnectorConfiguration):
        # Set the dataset ID
        self.catalog_id = config.catalog_id
        # Loads the custom configuration
        self.contact_points = config.connection_data["contact_points"]
        self.port = config.connection_data["port"]
        self.username = config.connection_data["username"]
        self.password = config.connection_data["password"]
        self.keyspace = config.connection_data["keyspace"]
        print(f"config connection_data: {config.connection_data}")

    def connect(self):
        try:
            logger.info("Connecting to Cassandra...")
            auth_provider = None
            if self.username and self.password:
                auth_provider = PlainTextAuthProvider(
                    username=self.username,
                    password=self.password
                )

            self.cluster = Cluster(
                contact_points=self.contact_points,
                port=self.port,
                auth_provider=auth_provider
            )
            self.session = self.cluster.connect(self.keyspace)
            logger.info("Connection to Cassandra established successfully.")
        except Exception as e:
            logger.error(f"Error connecting to Cassandra: {e}")
            raise

    def fetch_metadata(self) -> dict:

        # Connect to Cassandra
        self.connect()

        # Fetch metadata from the connector
        keyspace = self.cluster.metadata.keyspaces.get(self.keyspace)
        logger.info(keyspace)
        if not keyspace:
            raise ValueError("Keyspace {self.keyspace} does not exist.")

        tables_info = []

        # Loop through each table in the keyspace
        for table_name, table in keyspace.tables.items():
            # Collect column details
            columns = []
            for column_name, column in table.columns.items():
                columns.append({
                    "name": column_name,
                    "description": "",  # (optional) Fill in if you have column comment (read only)
                    "metadata": {
                        "title": column_name,  # mandatory
                        "description": "",  # (optional) Fill in if you have column comment (read only)
                        "creator": "",  # how can I get this?
                        "created": "",  # how can I get this?
                        "dataType": str(column.cql_type),
                        "position": 0
                    }
                })

            table_info = {
                "name": table_name,
                "metadata": {
                    "schema": self.keyspace,
                    "title": table_name,  # mandatory
                    "description": "",  # (optional) Fill in if you have column comment (read only)
                    "creator": "",  # how can I get this?
                    "created": "",  # how can I get this?
                },
                "columns": columns
            }
            tables_info.append(table_info)

        # Constructing the final object
        random_number = random.randint(0, 99999)
        random_number_with_padding = str(random_number).zfill(5)
        title = f"{self.keyspace} Database Metadata"
        database = {
            "catalogId": self.catalog_id,  # the API updates this automatically based on accessUrl (must be unique)
            "name": f"Database {self.keyspace} from pipeline (updated from pipeline)",
            "tables": tables_info,
            "metadata": {
                "title": title,
                "type": "Cassandra"
            },
            "distributions": [
                {
                    "title": title,
                    # mandatory (this attribute is used to name columns with a unique identifier & to identify the
                    # database of the dataset)
                    "accessUrl": f"jdbc:cassandra://{self.contact_points[0]}:{self.port}/{self.keyspace}?" +
                                 f"localdatacenter=datacenter1&dummy={random_number_with_padding}",
                    "byteSize": 1024
                }
            ]
        }

        return database

    def release_connection(self):
        # Releases the connection
        if self.session:
            logger.info(f"Releasing connection to Cassandra")
            self.session.shutdown()
            self.cluster.shutdown()
