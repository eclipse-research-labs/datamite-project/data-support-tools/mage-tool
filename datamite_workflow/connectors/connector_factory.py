"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import importlib
import inspect
import pkgutil
from typing import Callable
from .base_connector import BaseConnector
from .config.connector_configuration import ConnectorConfiguration

class ConnectorFactory:

    def __init__(self):
        self._reload_connectors('connectors')

    # Internal registry of connectors
    registry = {}

    @classmethod
    def register(cls, name: str) -> Callable:
        """ Class method to register Connector class to the internal registry.
        Args:
            name (str): The name of the connector.
        Returns:
            The Connector class itself.
        """

        def register_wrapper(wrapped_class: BaseConnector) -> Callable:
            if name in cls.registry:
                print(f"Connector {name} already exists. Will replace it")
            cls.registry[name] = wrapped_class
            return wrapped_class

        return register_wrapper


    @classmethod
    def create_connector(self, name: str, config: ConnectorConfiguration) -> BaseConnector:
        """ Factory command to create the connector.

        This method gets the appropriate Connector class from the registry
        and creates an instance of it, while passing in the parameters
        given in ``kwargs``.

        Args:
            name (str): The name of the executor to create.
            config (ConnectorConfiguration): The configuration for the connector.

        Returns:
            An instance of the connector that is created. If the connector does not exist in the registry,
            None is returned and a message is printed.
        """
        if len(self.registry) == 0:
            self._reload_connectors(__package__) 
        
        if name not in self.registry:
            raise ValueError(f"Connector {name} does not exist in the registry")

        exec_class = self.registry[name]
        connector = exec_class(config)
        
        return connector

    @classmethod
    def _reload_connectors(self, package: str):
        """
        Reload all connector classes from the given package.

        This function discovers all modules in the specified package, imports each module,
        and iterates over its classes. If a class is decorated with @ConnectorFactory.register,
        it is registered in the ConnectorFactory's registry.

        Parameters:
        package (str): The name of the package from which to reload connector classes.

        Returns:
        None
        """        
        # Discover all modules in the package
        package_module = importlib.import_module(package)
        package_path = package_module.__path__

        for _, module_name, _ in pkgutil.iter_modules(package_path):
            full_module_name = f"{package}.{module_name}"
            module = importlib.import_module(full_module_name)

            # Iterate over classes in the module
            for name, obj in inspect.getmembers(module, inspect.isclass):
                # Check if the class has been decorated with @ConnectorFactory.register
                if hasattr(obj, '_connector_name'):
                    connector_name = getattr(obj, '_connector_name')
                    self.register(connector_name)(obj)

