"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import random
from typing import Any, Dict, List, Optional
from pydantic import BaseModel
import pandas as pd

from .config.connector_configuration import ConnectorConfiguration
from datamite_workflow.utils.streaming import config as streaming_config
from datamite_workflow.utils.streaming.quality_management import evaluate_data
from .connector_factory import ConnectorFactory
from .base_connector import BaseConnector
import psycopg2


class ColumnMetadata(BaseModel):
    name: str
    description: Optional[str] = None
    creator: Optional[str] = None
    created: Optional[str]  = None
    data_type: str
    position: int
    

class TableMetadata(BaseModel):
    name: str
    description: Optional[str] = None
    creator: Optional[str] = None
    created: Optional[str] = None


def evaluate_column_metrics(column, df, table):
    column_sampling = df[column.name].to_numpy()
    # print(f"Column sampling: {column_sampling}")
    try:
        metrics = evaluate_data(data=column_sampling,
                                dataset="",
                                resource_name=f"table_{table.name}-column_{column.name}",
                                column_name=column.name,
                                column_type=column.data_type)
        quality_measurements = None
        if metrics is not None:
            quality_measurements = []
            for key, value in metrics.items():
                quality_measurements.append({
                    'name': f'{column.name}_{key}',
                    'metric': key,
                    'value': value,
                    'createdBy': '5bd93f4b-13d9-4567-a44d-9baa0eaee62a'
                })
        return quality_measurements
    except:
        return None


@ConnectorFactory.register("postgresql")
class PostgreSQLConnector(BaseConnector):

    def load_config(self, config: ConnectorConfiguration):

        # Validate Configuration before connecting
        if not config.connection_data:
            raise ValueError("Connection data is required.")

        # Loads the custom configuration
        self.host = config.connection_data["host"]
        self.port = config.connection_data["port"]
        self.username = config.connection_data["username"]
        self.password = config.connection_data["password"]
        self.database = config.connection_data["database"]
        self.schema = config.connection_data["schema"]
        self.connection = None

    def connect(self):

        try:
            self.connection = psycopg2.connect(
                host=self.host,
                port=self.port,
                dbname=self.database,
                user=self.username,
                password=self.password
            )

         # validate connection
            if self.connection is None:
                raise ValueError(
                    "Connection to PostgreSQL is not established.")

            self.cursor = self.connection.cursor()

        except Exception as e:
            raise ValueError(f"Error connecting to PostgreSQL: {e}")

    def fetch_metadata(self) -> dict:
        """
        Retrieves metadata for all tables in the specified schema, and
        builds the nested dictionary object (database) as required.
        """
        # print("fetching metadata")
        self.connect()

        if self.schema is None:
            raise ValueError("Schema name is required.")

        tables_info = []

        # 1) Get all tables in the schema
        tables = self._get_tables_in_schema(self.schema)
        print(f"Tables info: {tables}")
        # Alternatively, you can exclude certain tables if needed:
        # tables = self._get_tables_in_schema(self.schema, exclude_tables=["some_table"])
        for table in tables:

            # 2) Get column metadata for each table
            columns = self._get_table_metadata(self.schema, table.name)
            df = self.fetch_table_sampling_data(table)
            columns_info = []
            for column in columns:
                # column is a ColumnMetadata model
                print(f"Working with table {table.name} column {column.name}")
                quality_measurements = evaluate_column_metrics(column, df, table)
                print(f"Quality measurements {quality_measurements}")
                column_info = {
                    "name": column.name,
                    "description": column.description,
                    "metadata": {
                        "title": column.name,
                        "description": column.description,
                        "creator": column.creator,
                        "created": column.created,
                        "dataType": column.data_type,
                        "position": column.position,
                    },
                    "qualityMeasurements": quality_measurements
                }
                columns_info.append(column_info)

            table_info = {
                "name": table.name,
                "metadata": {
                    "schema": self.schema,
                    "title": table.name,
                    "description": table.description,
                    "creator": table.creator,
                    "created": table.created,
                },
                "columns": columns_info
            }
            tables_info.append(table_info)

        # 3) Construct the final object
        random_number = random.randint(0, 99999)
        random_number_with_padding = str(random_number).zfill(5)

        database = {
            # the API updates this automatically based on accessUrl (must be unique)
            "catalogId": self.catalog_id,
            "name": "Database from pipeline (updated from pipeline)",
            "tables": tables_info,
            "metadata": {
                "title": "Database Metadata",  # mandatory
                "type": "PostgreSQL"
            },
            "distributions": [
                {
                    "accessUrl": f"jdbc:postgresql://localhost:5432/database_test{random_number_with_padding}",
                    "title": "Title (must be optional)",
                    "byteSize": 1024
                }
            ]
        }

        return database

    def fetch_table_sampling_data(self, table):
        query_sampling = f"""
                SELECT *
                FROM {self.schema}.{table.name}
                ORDER BY random()
                LIMIT {streaming_config.sampling_limit}
            """
        df = pd.read_sql_query(query_sampling, self.connection)
        return df

    def release_connection(self):
        self.connection.close()

    def open_cursor(self):
        if self.cursor is None:
            self.cursor = self.connection.cursor()
        return self.cursor

    def _get_tables_in_schema(self, schema_name: str, exclude_tables: Optional[List[str]] = None) -> List[TableMetadata]:
        """
        Retrieves all tables in the specified schema, optionally excluding a set of table names.

        :param schema_name: The schema to search in, e.g., 'public'.
        :param exclude_tables: An optional list of table names to exclude.
        :return: A list of table names in the given schema, excluding any named in exclude_tables.
        """
        # Base query: get the names of all user-defined (BASE) tables in the schema
        query = """
            SELECT
                t.table_name,
                pg_get_userbyid(c.relowner)            AS table_creator,
                obj_description(c.oid, 'pg_class')     AS table_description,
                NULL                                   AS table_created  
            FROM information_schema.tables t
            JOIN pg_class c
                ON c.relname = t.table_name
            JOIN pg_namespace nsp
                ON nsp.oid = c.relnamespace
            AND nsp.nspname = t.table_schema
            WHERE t.table_schema = %s
            AND t.table_type = 'BASE TABLE'
        """

        params = [schema_name]

        # If exclude_tables is provided, add a WHERE NOT IN clause
        if exclude_tables:
            # Dynamically build the placeholders for each excluded table
            placeholders = ','.join(['%s'] * len(exclude_tables))
            query += f" AND table_name NOT IN ({placeholders})"
            params.extend(exclude_tables)

        # Sort the tables by name for consistent ordering
        query += " ORDER BY table_name"

        self.cursor.execute(query, params)
        rows = self.cursor.fetchall()

        # Each row returns:
        #   [ table_name, table_creator, table_description, table_created ]
        tables = []
        for row in rows:
            table_name = row[0]
            table_creator = row[1] or ""
            table_description = row[2] or ""
            table_created = row[3] or ""
            
            # Build TableMetadata object and append to tables
            table_metadata = TableMetadata(
                 name=table_name, 
                 description=table_description, 
                 creator=table_creator, 
                 created=table_created
                 )
            tables.append(table_metadata)

        return tables

    def _get_table_metadata(self, schema_name: str, table_name: str) -> List[ColumnMetadata]:
        """
        Retrieves column metadata (name, description, creator, created, dataType, position)
        for the specified table from PostgreSQL and returns a list of ColumnMetadata objects.
        """
        query = """
            SELECT
                c.column_name                                  AS "name",
                col_description(tbl.oid, c.ordinal_position)   AS "description",
                pg_get_userbyid(tbl.relowner)                  AS "creator",
                NULL                                           AS "created",  
                c.data_type                                    AS "dataType",
                c.ordinal_position                             AS "position"
            FROM information_schema.columns c
            JOIN pg_class tbl
                ON  tbl.relname = c.table_name
            JOIN pg_namespace nsp
                ON  nsp.oid = tbl.relnamespace
            AND nsp.nspname = c.table_schema
            WHERE c.table_schema = %s
            AND c.table_name   = %s
            ORDER BY c.ordinal_position;
        """

        self.cursor.execute(query, (schema_name, table_name))
        rows = self.cursor.fetchall()

        print(
            f"number of rows: {len(rows)} for table: {table_name} and schema: {schema_name}")

        # Transform rows into ColumnMetadata models
        metadata_list = []
        for row in rows:
            # row order: [ name, description, creator, created, dataType, position ]
            metadata_list.append(
                ColumnMetadata(
                    name=row[0],
                    description=row[1],
                    creator=row[2],
                    created=row[3],  
                    data_type=row[4],
                    position=row[5]
                )
            )
        return metadata_list
