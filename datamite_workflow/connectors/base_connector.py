"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
from abc import ABC, abstractmethod

from .config.connector_configuration import ConnectorConfiguration


class BaseConnector(ABC):
            

    def __init__(self, config: ConnectorConfiguration):
        """
        Initialize the BaseConnector instance with configuration and dataset ID.

        Parameters:
        config (ConnectorConfiguration): The configuration for the connector.

        Returns:
        None
        """
        
        if config.catalog_id is None:
            raise ValueError("Catalog ID is required.")
               
        self.config = self.load_config(config)
        
        self.catalog_id = config.catalog_id
        
    @abstractmethod
    def load_config(self, config: ConnectorConfiguration):
        """Loads the custom configuration"""
        pass

    @abstractmethod
    def fetch_metadata(self) -> dict:
        """Fetches metadata from the connector"""
        pass

    @abstractmethod
    def connect(self):
        """Fetches metadata from the connector"""
        pass

    @abstractmethod
    def release_connection(self):
        """Releases the connection"""
        pass