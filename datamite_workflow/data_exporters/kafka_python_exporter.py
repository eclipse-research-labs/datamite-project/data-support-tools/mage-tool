"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
from mage_ai.streaming.sinks.base_python import BasePythonSink
from typing import Callable, List, Dict
from kafka import KafkaProducer
import json


if 'streaming_sink' not in globals():
    from mage_ai.data_preparation.decorators import streaming_sink


class KafkaClient:
    def __init__(self, bootstrap_servers, topic):
        self.producer = KafkaProducer(
            bootstrap_servers=bootstrap_servers,
            value_serializer=lambda v: json.dumps(v).encode('utf-8')
        )
        print("Producer created")
        self.topic = topic

    def send_messages(self, messages):
        # print("inside send_messages")
        if not isinstance(messages, list):
            raise ValueError("messages must be a list")

        for message in messages:
            self.producer.send(self.topic, value=message)
            self.producer.flush()

@streaming_sink
class CustomSink(BasePythonSink):
    def __init__(self, **kwargs):
        self.init_variables(**kwargs)
        super().__init__(**kwargs)

    def init_client(self):
        """
        Implement the logic of initializing the client.
        """

    def init_variables(self, **kwargs):
        print(f"Kafka Initializing variables {kwargs}")
        self.kwargs = kwargs
        vbles = self.kwargs['global_vars']
        kafka_host = vbles['kafka_host']
        kafka_port = vbles['kafka_port']
        kafka_topic = vbles['kafka_topic']
        #'kafka_host': '192.168.20.143', 'kafka_port': 9093, 'kafka_topic': 'mqtt-pipeline-enhanced',
        broker = f"{kafka_host}:{kafka_port}"
        print(f"Connecting to kafka broker {broker}")
        self.kafka_client = KafkaClient(bootstrap_servers=[f"{broker}"], topic= kafka_topic)

    def batch_write(self, messages: List[Dict]):
        """
        Batch write the messages to the sink.

        For each message, the message format could be one of the following ones:
        1. message is the whole data to be wirtten into the sink
        2. message contains the data and metadata with the foramt {"data": {...}, "metadata": {...}}
            The data value is the data to be written into the sink. The metadata is used to store
            extra information that can be used in the write method (e.g. timestamp, index, etc.).
        """

        self.kafka_client.send_messages(messages)

