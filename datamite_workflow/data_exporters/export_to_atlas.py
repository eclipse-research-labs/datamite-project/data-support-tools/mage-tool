"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import requests
import json

from datamite_workflow.utils.streaming.error_management import notify_error, notify_success
from datamite_workflow.utils.streaming import config

GOVERNANCE_URL = config.governance_service

if 'data_exporter' not in globals():
    from mage_ai.data_preparation.decorators import data_exporter


# Send to atlas
def send_database_metadata_atlas(database):
    # URL del endpoint
    endpoint_url = f'{GOVERNANCE_URL}/databases'
    print(endpoint_url)
    send_to_atlas(database, endpoint_url)


def send_columns_metadata_atlas(columns):
    # URL del endpoint
    endpoint_url = f'{GOVERNANCE_URL}/columns'
    print(endpoint_url)
    for column in columns:
        # Convert to json
        send_to_atlas(column, endpoint_url)


def send_to_atlas(data, endpoint_url):
    metadata_json = json.dumps(data)
    # HTTP headers
    headers = {"Content-Type": "application/json"}
    try:
        # Post request
        response = requests.post(endpoint_url, data=metadata_json, headers=headers)
        print(response)
        # Verify response
        if 200 <= response.status_code < 300:
            print("OK")
        else:
            print(f"Error code {response.status_code}")
            print(response.text)
            print(f'Payload: {metadata_json}')
    except Exception as e:
        print(f"Error sending to endpoint: {e}")


@data_exporter
def export_data(data, *args, **kwargs):
    """
    Exports data to some source.

    Args:
        data: The output from the upstream parent block
        args: The output from any additional upstream blocks (if applicable)

    Output (optional):
        Optionally return any object and it'll be logged and
        displayed when inspecting the block run.
    """
    print("Exporting data")
    print(data)
    content_type = data.get('content_type')
    try:
        if content_type is None:
            raise RuntimeError('Missing Atlas Content-type')
        if content_type == 'columns':
            send_columns_metadata_atlas(data.get('columns'))
        elif content_type == 'database':
            send_database_metadata_atlas(data['database'])
        else:
            raise RuntimeError(f'Unknown Atlas Content-type {content_type}')
        notify_success(data.get('streaming_id'))
    except Exception as error:
        notify_error(data.get('streaming_id'), f"{error}")


