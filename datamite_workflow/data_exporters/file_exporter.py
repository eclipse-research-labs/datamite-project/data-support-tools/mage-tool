"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import json
import os
import time
from datetime import datetime
from typing import List, Dict

from mage_ai.streaming.sinks.base_python import BasePythonSink
from minio import Minio
from minio.error import S3Error

from datamite_workflow.utils.streaming.artifact_management.services import streaming_service

if 'streaming_sink' not in globals():
    from mage_ai.data_preparation.decorators import streaming_sink


def get_filtered_message(data, include_timestamp=False, include_topic=False):
    parsed_data = json.loads(data)
    message = parsed_data.get('message')
    timestamp = parsed_data.get('timestamp')
    topic = parsed_data.get('topic')
    if not include_timestamp and not include_topic:
        return json.dumps(message)
    result = message
    if include_timestamp:
        result['_timestamp'] = timestamp
    if include_topic:
        result['_topic'] = topic
    return json.dumps(result)


@streaming_sink
class CustomSink(BasePythonSink):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.last_consolidation_time = None
        self.init_variables(**kwargs)

    def init_variables(self, **kwargs):
        """
        Implement the logic of initializing the client.
        """
        print(f"Kafka Initializing variables {kwargs}")
        self.kwargs = kwargs
        vbles = self.kwargs['global_vars']
        self.parameters = vbles
        self.file_prefix = vbles['file_prefix']
        self.streaming_id = vbles['streaming_id']
        self.max_chunk_size = vbles['max_chunk_size']
        self.consolidation_type = vbles['consolidation_type']
        self.consolidation_value = vbles['consolidation_value']
        self.minio_client = Minio(
            vbles['minio_host'],
            access_key=vbles['minio_key'],
            secret_key=vbles['minio_secret'],
            secure=False
        )
        self.chunk_path = f"/chunk-store/{self.streaming_id}"
        if not os.path.exists(self.chunk_path):
            os.makedirs(self.chunk_path)
        self.current_file = None
        self.current_file_name = None
        self.current_size = 0
        self.last_consolidation_time = time.time()
        self.open_new_file()

    def open_new_file(self):
        self.current_minute = datetime.now().minute
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        self.current_file_name = f"/{self.chunk_path}/chunk_{timestamp}.txt"
        self.current_file = open(self.current_file_name, "w")
        self.current_size = 0

    def close_current_file(self):
        if self.current_file:
            self.current_file.close()

    def check_file_change(self):
        if (self.current_size / 1024) >= self.max_chunk_size:
            self.close_current_file()
            self.open_new_file()

    def should_consolidate(self):
        print(
            f"{time.time() - self.last_consolidation_time} seconds since last consolidation / current size {self.current_size}")
        if self.consolidation_type == 'TIME':
            return time.time() - self.last_consolidation_time >= self.consolidation_value
        elif self.consolidation_type == 'SIZE':
            total_size = self.current_size + sum(
                os.path.getsize(f"{self.chunk_path}/{file}") for file in os.listdir(self.chunk_path) if
                file != self.current_file_name)
            return (total_size / 1024) >= self.consolidation_value
        return False

    def consolidate_and_upload(self):
        print("consolidate_and_upload")
        self.close_current_file()
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        consolidated_file_path = f"{self.chunk_path}/{self.file_prefix}_{timestamp}.txt"

        with open(consolidated_file_path, "w") as consolidated_file:
            for chunk_file_name in os.listdir(self.chunk_path):
                chunk_file_path = os.path.join(self.chunk_path, chunk_file_name)
                if chunk_file_path != consolidated_file_path:
                    with open(chunk_file_path, "r") as chunk_file:
                        for line in chunk_file:
                            consolidated_file.write(line)
                    os.remove(chunk_file_path)

        try:
            artifact_size = os.path.getsize(consolidated_file_path)
            artifact = streaming_service.get_or_create_active_artifact(self.streaming_id, artifact_size)
            with open(consolidated_file_path, "rb") as file_data:
                self.minio_client.put_object(
                    bucket_name=artifact.location.bucket_name,
                    object_name=f"{artifact.location.key_path}/{artifact.info.resource_name}".lstrip('/'),
                    data=file_data,
                    length=artifact_size,
                    content_type="application/json"
                )
            print(f"Archivo {consolidated_file_path} subido a Minio.")
            streaming_service.consolidate_artifact(self.streaming_id, artifact.info.artifact_id, consolidated_file_path)
        except S3Error as err:
            print(f"Error al subir archivo a Minio: {err}")

        os.remove(consolidated_file_path)
        self.last_consolidation_time = time.time()
        self.open_new_file()

    def batch_write(self, messages: List[Dict]):
        print(f"batch_write {messages}")
        for msg in messages:
            self.write_message(msg)
            self.check_file_change()
            if self.should_consolidate():
                self.consolidate_and_upload()

    def write_message(self, message: Dict):
        filtered_message = get_filtered_message(message,
                                                self.parameters['include_timestamp'],
                                                self.parameters['include_topic'])
        message_size = len(json.dumps(filtered_message)) + 1  # tamaño en bytes +1 por el retorno de carro
        self.current_file.write(f"{filtered_message}\n")
        self.current_size += message_size

    def __del__(self):
        self.consolidate_and_upload()
        self.close_current_file()
