"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
from mage_ai.streaming.sources.base_python import BasePythonSource
from typing import Callable
import paho.mqtt.client as mqtt
import logging, json
from datetime import datetime
from datamite_workflow.utils.streaming.error_management import notify_error

logger = logging.getLogger(__name__)

if 'streaming_source' not in globals():
    from mage_ai.data_preparation.decorators import streaming_source


@streaming_source
class CustomSource(BasePythonSource):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.init_variables(**kwargs)

    def init_variables(self, **kwargs):
        """
        Implement the logic of initializing the client.
        """
        print(f"MQTT Initializing variables {kwargs}")
        self.kwargs = kwargs

    def batch_read(self, handler: Callable):
        """
        Batch read the messages from the source and use handler to process the messages.
        """
        vbles = self.kwargs['global_vars']
        mqtt_host = vbles['mqtt_host']
        mqtt_port = vbles['mqtt_port']
        mqtt_topic = vbles['topic']
        streaming_id = vbles['streaming_id']

        client = mqtt.Client()

        def on_message(client, userdata, message):
            # print(f"Mensaje recibido: {message.payload.decode()} en el topic {message.topic}")
            message_data = {
                "topic": message.topic,
                "message": json.loads(message.payload.decode("utf-8")),
                "timestamp": datetime.now().isoformat()
            }
            handler([message_data])

        def on_subscribe(client, userdata, mid, granted_qos):
            print(f"Suscrito al topic {mqtt_topic} con QoS {granted_qos}")

        def on_connect(client, userdata, flags, rc):
            if rc != 0:
                error_message = f"Failed to connect to MQTT broker at {mqtt_host}:{mqtt_port}, return code {rc}"
                logger.error(error_message)
                notify_error(streaming_id, error_message)
            else:
                client.subscribe(mqtt_topic)
                logger.info(f"Connected to MQTT broker at {mqtt_host}:{mqtt_port}")

        client.on_subscribe = on_subscribe
        client.on_message = on_message
        client.on_connect = on_connect

        try:
            client.connect(mqtt_host, int(mqtt_port), 60)
        except Exception as error:
            notify_error(streaming_id, f"{error}")
            return
        client.subscribe(mqtt_topic)
        logger.info(f"batch_read start {mqtt_host}:{mqtt_port} {mqtt_topic}")
        client.loop_forever()  # Mantener el bucle de escucha indefinidamente
        logger.info("batch_read end")
