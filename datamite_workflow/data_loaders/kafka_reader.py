"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
from mage_ai.streaming.sources.base_python import BasePythonSource
from typing import Callable, Dict
from kafka import KafkaConsumer
import logging
import os

logger = logging.getLogger(__name__)

if 'streaming_source' not in globals():
    from mage_ai.data_preparation.decorators import streaming_source


@streaming_source
class CustomKafkaSource(BasePythonSource):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.init_variables(**kwargs)

    def init_variables(self, **kwargs):
        """
        Implement the logic of initializing the client.
        """
        print(f"Kafka Initializing variables {kwargs}")
        self.kwargs = kwargs
        self.vbles = self.kwargs['global_vars']
       
        print(f"init_variables end")


    def batch_read(self, handler: Callable):
        """
        Batch read the messages from the source and use handler to process the messages.
        """
        print(f"batch_read start")

        kafka_host = self.vbles['kafka_host']
        kafka_port = self.vbles['kafka_port']
        kafka_topic = self.vbles['kafka_topic']

        self.consumer = KafkaConsumer(
            kafka_topic,
            bootstrap_servers=f'{kafka_host}:{kafka_port}',
            group_id='unique_reader2',
            auto_offset_reset='earliest',
            enable_auto_commit=True,
            value_deserializer=lambda m: m.decode('utf-8'),
            max_poll_interval_ms=3000

        )
        print(f"Consumer active in {kafka_host}:{kafka_port} {kafka_topic}")

        for message in self.consumer:
            #print(f"Read kakfa message: {message}")
            handler([message.value])

        print("batch_read end")
