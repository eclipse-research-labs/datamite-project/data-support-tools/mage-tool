"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 
import os
import traceback
from io import StringIO

from botocore import endpoint
from mage_ai.settings.repo import get_repo_path
from mage_ai.io.config import ConfigFileLoader, ConfigKey
from pandas import DataFrame

from datamite_workflow.utils.streaming.error_management import notify_error
from os import path
from datetime import datetime
import json
import uuid
import tempfile
import pandas as pd
from smart_open import open
import boto3

from datamite_workflow.utils.minio.general_methods import GeneralMethods
from datamite_workflow.utils.streaming.quality_management import evaluate_data
from datamite_workflow.utils.streaming.configuration import config


if 'data_loader' not in globals():
    from mage_ai.data_preparation.decorators import data_loader
if 'test' not in globals():
    from mage_ai.data_preparation.decorators import test


class MinIOConnector:
    def __init__(self, config_path, config_profile, artifact_id, connection_data):
        self.temp_file_path = None
        self.config = self.load_config(config_path, config_profile)
        self.artifact_id = artifact_id
        self.connection_data = connection_data
        self.bucket_name = 'default'
        self.session = None
        self.connect()

    def load_config(self, config_path, config_profile):
        return ConfigFileLoader(config_path, config_profile)

    def connect(self):
        self.session = boto3.client('s3',
                                    aws_access_key_id=self.config['AWS_ACCESS_KEY_ID'],
                                    aws_secret_access_key=self.config['AWS_SECRET_ACCESS_KEY'],
                                    endpoint_url=self.config['AWS_ENDPOINT']
                                    )

    def get_columns_metadata(self):
        df: DataFrame = self.get_dataframe()
        column_data_types = GeneralMethods.data_types(df)
        num_rows = df.shape[0]
        columns_info = []
        for index, column_data_type in enumerate(column_data_types):
            column_name = column_data_type['column_name']
            column_info = {
                # 'id': '',
                'name': column_name,
                'description': '',
                'metadata': {
                    'title': column_name,
                    'creator': self.config.get(ConfigKey.AWS_ACCESS_KEY_ID),
                    'created': int(datetime.now().timestamp()),
                    'dataType': column_data_type['value'],
                    'position': index,
                    'numRecords': num_rows,
                    'isPrivate': False
                },
                'qualityMeasurements': [],
                'structuredDatasetId': self.artifact_id
            }
            column = df.get(column_name)
            values = list(column)
            quality_dataset = os.path.splitext(self.connection_data['filename'])[0]
            if self.connection_data.get('quality_dataset') is not None:
                quality_dataset = self.connection_data.get('quality_dataset')
            metrics = evaluate_data(data=values,
                                    dataset=quality_dataset,
                                    resource_name=self.connection_data['filename'],
                                    column_name=column_name,
                                    column_type=column_data_type['value'])
            if metrics is not None:
                quality_measurements = []
                for key, value in metrics.items():
                    quality_measurements.append({
                        'name': f'{column_name}_{key}',
                        'metric': key,
                        'value': value,
                        'createdBy': '5bd93f4b-13d9-4567-a44d-9baa0eaee62a'
                    })
                column_info['qualityMeasurements'] = quality_measurements
            columns_info.append(column_info)
        return {
            'columns': columns_info
        }

    def get_dataframe(self) -> DataFrame:
        # Construir la ruta completa del archivo en el bucket de MinIO
        full_path = self.artifact_id + '/' + self.connection_data['filename']

        # Descargar el archivo desde MinIO a un objeto en memoria
        obj = self.session.get_object(Bucket='default', Key=full_path)
        data = obj['Body'].read().decode(self.connection_data['encoding'])

        # Crear un archivo temporal y escribir los datos en él
        with tempfile.NamedTemporaryFile(delete=False, suffix='.csv') as temp_file:
            temp_file.write(data.encode(self.connection_data['encoding']))
            temp_file_path = temp_file.name  # Obtener la ruta del archivo temporal

        # Ahora que el archivo está en el sistema de archivos local, leerlo con pandas
        df: DataFrame = pd.read_csv(temp_file_path,
                                    delimiter=self.connection_data['field_delimiter'],
                                    decimal=self.connection_data['decimal_delimiter'],
                                    encoding=self.connection_data['encoding']
                                    )
        self.temp_file_path = temp_file_path
        return df

    def clean(self):
        self.session.close()
        if self.temp_file_path is not None:
            os.remove(self.temp_file_path)
            self.temp_file_path = None


@data_loader
def load_data(*args, **kwargs):
    
    logger = kwargs.get('logger')
    logger.info("MinIO metadata extract loader..")
    
    # Check config URLs    
    logger.info(f"External URL Configuration: {config.to_dict()}")
    
    minio_connector = None
    # Get connection properties from yaml config file
    config_path = path.join(get_repo_path(), 'custom_config.yaml')
    config_profile = 'minio'
    
    # Get artifact_id from pipeline global vars.
    artifact_id = kwargs['artifact_id']
    streaming_id = kwargs['streaming_id']
    connection_info = kwargs['connection_info']
    connection_data = connection_info['connection_data']
    try:
        minio_connector = MinIOConnector(config_path, config_profile, artifact_id, connection_data)

        data = minio_connector.get_columns_metadata()
        data['artifact_id'] = artifact_id
        data['streaming_id'] = streaming_id
        data['content_type'] = 'columns'
        return data
    except Exception as error:
        print(error)
        traceback.print_exc()
        notify_error(streaming_id, f"{error}")
    finally:
        minio_connector.clean()


@test
def test_output(output, *args) -> None:
    assert output is not None, 'The output is undefined'
