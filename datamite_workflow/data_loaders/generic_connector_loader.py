"""MIT License

Copyright (c) 2024 Jerónimo Pla - ITI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""
 

from datamite_workflow.connectors.config.connector_configuration import ConnectorConfiguration, DatabaseType
from datamite_workflow.connectors.connector_factory import ConnectorFactory
from datamite_workflow.utils.streaming.error_management import notify_error

if 'data_loader' not in globals():
    from mage_ai.data_preparation.decorators import data_loader
if 'test' not in globals():
    from mage_ai.data_preparation.decorators import test


@data_loader
def load_metadata(*args, **kwargs):
    """
    Load metadata using a connector
    """
    
    catalog_id = kwargs["catalog-id"]
    session_id = kwargs["session-id"]
    conn_info = kwargs["connection-info"]

    database_type = conn_info['type']
    
    # TODO: implement a validator for the database type (available connectors)
    if not database_type:
        raise ValueError("Database type is required in order to instantiate a connector.")
    
    print(f"Connection type: {database_type}")

    # Read and validate connection information
    config = ConnectorConfiguration()
    config.catalog_id = catalog_id
    config.connection_data = conn_info["connection-data"]

    # Check for available connector based on type attribute
    conn = None
    try:
        conn = ConnectorFactory.create_connector(database_type, config)
        metadata = conn.fetch_metadata()
    except Exception as e:
        notify_error(session_id, f"Failed to fetch metadata: {str(e)}")
        raise e
    finally:
        # Release the connection
        if conn:
            conn.release_connection()
    data = {
        'database': metadata,
        'streaming_id': session_id,
        'content_type': 'database'
    }
    return data


@test
def test_output(output, *args) -> None:
    """
    Template code for testing the output of the block.
    """
    print("Output: {}".format(output))
    assert output is not None, 'The output is Not None'