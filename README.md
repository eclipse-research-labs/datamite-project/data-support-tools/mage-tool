# Data Discovery Connectors based on MageAI pipelines

## 1. Introduction

This is a repo with a data discover connector for different Datasets within a workflow orchestration with MageAI

The idea is to create different pipelines for these Datasets in order to:
- Extract the metadata of these DS for insertion into ATLAS via the Governance API.
- Evaluate the quality of the different DS


## 2. Basic Requirements

Before you begin setting up the project, ensure that you meet the following prerequisites to smoothly run and test the application:

- **Docker**: You should have Docker installed and be familiar with its basic operations. For installation instructions, visit [Docker's official website](https://www.docker.com/get-started).

- **Git**: Git must be installed on your machine as it is used for version control and for obtaining the latest code from the repository. If you need to install Git, you can find the instructions on the [Git website](https://git-scm.com/downloads).

- **Optional - Containerized Database**: Although not mandatory, having a containerized database such as MySQL or PostgreSQL can be beneficial for testing database interactions. If you choose to use a containerized database, Docker will be used to run the database instance. Instructions on how to set up a containerized database will be provided in the setup steps.

Ensure that your system meets these requirements before proceeding to the installation and configuration steps.

## 3. Step-by-Step Guidelines

### Step 1: Setup

1. **Clone the repository**:
    ```bash
    git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/mage-tool
    cd mage-tool
    ```

2. **Create a new Git branch**:
    Here's how to create a new branch for your specific changes, which is a good practice to keep your developments organized and separate from the main project line:
    ```bash
    git checkout -b your-branch-name
    ```
    Replace `your-branch-name` with a descriptive name for your branch, for example, `feature/team-name`.


### Step 2: Configuration

**Configure database connection settings**:

Update the `io_config.yaml` file in the root directory of your project with the necessary connection details based on the target DB. Here is an example of what the configuration might look like for PostgreSQL:

```yaml
version: 0.1.1
 default:
    POSTGRES_CONNECT_TIMEOUT: <Your Connection Timeout> 
    POSTGRES_DBNAME: <Your Database Name> 
    POSTGRES_SCHEMA: <Your Database Schema> # e.g., 'public', optional
    POSTGRES_USER: <Your Database Username> 
    POSTGRES_PASSWORD: <Your Database Password> 
    POSTGRES_HOST: <Your Database Host> 
    POSTGRES_PORT: <Your Database Port> # typically 5432    
 ```

If you are using a database not natively supported by Mage, such as Elasticsearch, you will need to create a custom configuration file. Here's an example setup:

```yaml
version: 0.1.1
  custom:
    ELASTICSEARCH_HOST: <Your Elasticsearch Host> # e.g., 'localhost'
    ELASTICSEARCH_PORT: <Your Elasticsearch Port> # typically 9200
    ELASTICSEARCH_USER: <Your Elasticsearch Username>
    ELASTICSEARCH_PASSWORD: <Your Elasticsearch Password>
```

This custom configuration file should be placed in the root directory of your project. Update this file with your Elasticsearch details to ensure proper connection. 


### Step 3: Running the Project

1. **Start the project using Docker**:

    To get your project up and running with all its dependencies in a Docker environment, run the following command in your terminal:

    ```bash
    docker-compose up -d
    ```
    This command starts all the services defined in your `docker-compose.yml` file in detached mode, which means they'll run in the background.

2. **Access the application**:
    
    Once the Docker containers are running, you can access the application by navigating to the following URL in your web browser:

    [http://localhost:6789/overview](http://localhost:6789/overview)
    This link will take you to the Home Page of the Application. In this case, authentication is disabled setting `REQUIRE_USER_AUTHENTICATION` environment variable to `0` ([MageAI User Authentication reference](https://docs.mage.ai/production/authentication/overview)).

### Step 3: Creating the Dataset via the Governance API

 Before creating a new Pipeline, you must create a new Dataset using the Governance API. 
    
 Use the following `curl` command to send a POST request to the server in order to add a new Dataset:

 ```bash
 curl -X POST http://91.235.109.231:8091/datasets/pipeline \
      -H "Content-Type: application/json" \
      -d '{"name": "<put your dataset name here>"}'
 ```
 Sample expected result:
 ```
 "1ee9b357-29c1-41ac-8a8e-8f5ff0f07c56"
 ```
 This UUID is a unique identifier for the dataset you just added and will be used for further interactions with the pipeline:


### Step 4: Creating and running my first pipeline

1. **Create new pipeline**:

   From the MageAI Web interface (for a local installation follow the link: [http://localhost:6789/overview](http://localhost:6789/overview)) create a new pipeline (Standard batch pipeline). 


2. **Add Data Loader**:

   Select Generic Python data loader from the dropdown menu and give a descriptive name.

3. **Implement the Data Loader**:

    Insert the Python code into the functions templates (@data_loader & @test).

    ```python	
    @data_loader
    def load_data(*args, **kwargs):
        """
        Template code for loading data from any source.

        Returns:
            Anything (e.g. data frame, dictionary, array, int, str, etc.)
        """
        # Specify your data loading logic here

        return {}


    @test
    def test_output(output, *args) -> None:
        """
        Template code for testing the output of the block.
        """
        assert output is not None, 'The output is undefined'
    ```	

4. **Run the pipeline**:

    You can run the pipeline from the upper menu of the pipeline step box:
    - Press the Play icon to run the pipeline step.
    - Execute block and run tests option of the dropdown menu from the upper toolbar.


### Step 5: Check the results

After adding a dataset to your system and run your pipeline you can verify its status or retrieve its details using a simple `GET` request. 

```bash	
curl -X 'GET' \
 'http://91.235.109.231:8091/datasets/<your Dataset UUID here>' \
 -H 'accept: application/json'
 ```	

